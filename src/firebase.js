import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyCh3m5TllVICG1P5KhCg3NnG-jHAz3OYHM",
    authDomain: "twitter-clone-f475c.firebaseapp.com",
    projectId: "twitter-clone-f475c",
    storageBucket: "twitter-clone-f475c.appspot.com",
    messagingSenderId: "980142880801",
    appId: "1:980142880801:web:4a1809ca61d367ceb48b60",
    measurementId: "G-QZE0L9LH3K"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig)

  const db =firebaseApp.firestore()

  export default db