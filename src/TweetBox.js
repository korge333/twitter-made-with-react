import React,{useState} from "react"
import "./TweetBox.css"
import {Avatar, Button } from "@material-ui/core"
import db from "./firebase"

function TweetBox(){
    const[tweetMessage, setTweetMessage]=useState("")
    const[tweetImage, setTweetImage]=useState("")

    const sendTweet = (e) => {
        e.preventDefault()
        db.collection("posts").add({
            displayName: "Jorge Costa",
            userName:"jorgecosta",
            verified: true,
            text: tweetMessage,
            image: tweetImage,
            avatar:""
        })
        setTweetImage("")
        setTweetMessage("")
    }

    return(
        <div className="tweetBox">
            <form>
                <div className ="tweetBox__input">
                <Avatar/>
                <input 
                    onChange={e=>setTweetMessage(e.target.value)}
                    value ={tweetMessage}
                    placeholder="What's happening?" 
                    type="text">
                </input>
                </div>
                <input 
                    value={tweetImage}
                    onChange={e => setTweetImage(e.target.value)}
                    className ="tweetBox__imageInput" 
                    placeholder="Enter image" 
                    type="text">
                </input>

                <Button 
                    onClick={sendTweet}
                    type= "submit" 
                    className="tweetBox__tweetButton">
                        Tweet
                </Button>
            </form>
        </div>
    )
}

export default TweetBox